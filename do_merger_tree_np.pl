#!/usr/bin/perl -w

my $dir = $ARGV[0];
my $cfg = $ARGV[1];
check_system(sprintf("%s/gravitational_consistency_no_periodic", $dir), $cfg);
check_system(sprintf("%s/find_parents_and_cleanup_no_periodic", $dir), $cfg);
check_system(sprintf("%s/resort_outputs", $dir), $cfg);
check_system(sprintf("%s/assemble_halo_trees", $dir), $cfg);

sub check_system {
    system(@_) == 0 or
	die "Tree creation failed while executing \"@_\".\n(See errors above).\n";
}
